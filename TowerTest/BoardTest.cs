﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tower;

namespace TowerTest
{
    [TestClass]
    public class BoardTest
    {
        [TestMethod]
        public void NormalGame_StartingMonsterDiscard()
        {
            Board b = Factory_Create2PlayerVSBoard();
            int expected = 48;
            int actual = b.MonsterDiscard.Count(); //All cards are in discard until the first draw
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NormalGame_StartingMonsterDeckHasProperNumHellHounds()
        {
            Board b = Factory_Create2PlayerVSBoard();
            int expected = 8;
            int actual = b.MonsterDiscard.Count(z => z.Name == "Hellhound");
            Assert.AreEqual(expected, actual);
        }

        private Board Factory_Create2PlayerVSBoard()
        {
            return new Board(2);
        }
    }
}
