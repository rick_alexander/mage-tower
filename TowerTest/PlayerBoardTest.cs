﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tower;
using Tower.Monsters;

namespace TowerTest
{
    [TestClass]
    public class PlayerBoardTest
    {
        [TestMethod]
        public void PlayerBoard_StartingCards()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            int expected = 2;
            int actual = pb.Hand.Count();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod]
        //public void PlayerBoard_StartingCards_Plus_Unicorn()
        //{
        //    PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
        //    int expected = 3;
        //    int actual = pb.Hand.Count();
        //    Assert.AreEqual(expected, actual);
        //}

        //[TestMethod]
        //public void PlayerBoard_StartingCards_Plus_Bonus()
        //{
        //    PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
        //    int expected = 4;
        //    int actual = pb.Hand.Count();
        //    Assert.AreEqual(expected, actual);
        //}

        //[TestMethod]
        //public void PlayerBoard_StartingCards_Plus_Bonus_And_Unicorn()
        //{
        //    PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
        //    int expected = 5;
        //    int actual = pb.Hand.Count();
        //    Assert.AreEqual(expected, actual);
        //}

        [TestMethod]
        public void PlayerBoard_StartingMonsters()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            int expected = 0;
            int actual = pb.MonsterQueue.Count();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PlayerBoard_StartingGold()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            int expected = 0;
            int actual = pb.Gold;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PlayerBoard_NewTurn_AddsGold()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            int expected = pb.Gold + 1;
            StartBoardTurn(pb);
            int actual = pb.Gold;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PlayerBoard_NewTurn_AddsCards()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            int expected = pb.Hand.Count + 2;
            StartBoardTurn(pb);
            int actual = pb.Hand.Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PlayerBoard_NewTurn_AddsAtLeast10Power()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            StartBoardTurn(pb);
            int actual = pb.MonsterQueue.Select(z => z.Value).Sum();
            Assert.IsTrue(actual >= 10);
        }

        [TestMethod]
        public void PlayerBoard_NewTurn_AddsAtMost12Power()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            int actual = pb.MonsterQueue.Select(z => z.Value).Sum();
            Assert.IsTrue(actual <= 12);
        }

        [TestMethod]
        public void PlayerBoard_AngerLowersAngerCount()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateOrc() });
            Assert.IsTrue(pb.MonsterQueue[0].AngerCount == 2);
            pb.Anger();
            Assert.IsTrue(pb.MonsterQueue[0].AngerCount == 1);
        }

        [TestMethod]
        public void PlayerBoard_Hellhound_AttacksInOneTurn()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateHellhound() });
            Assert.IsTrue(pb.MonsterQueue.Count == 1);
            pb.Anger();
            Assert.IsTrue(pb.MonsterQueue.Count == 0);
        }

        [TestMethod]
        public void PlayerBoard_Warlock_AngerSubtractsLife()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateWarlock() });
            Assert.IsTrue(pb.Life == 20);
            pb.Anger();
            Assert.IsTrue(pb.Life == 19);
        }

        [TestMethod]
        public void PlayerBoard_Demon_AngerSpawnsDemonspawn()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateDemon() });
            Assert.IsTrue(pb.MonsterQueue.Count == 1);
            pb.Anger();
            Assert.IsTrue(pb.MonsterQueue.Count == 2);
            Assert.IsTrue(pb.MonsterQueue.Count(z => z.Name == "Demonspawn") == 1);
        }

        [TestMethod]
        public void PlayerBoard_Damage_NonLethalHitsStore()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateOrc() });
            pb.Damage(new Damage(1, false, false));
            Assert.IsTrue(pb.MonsterQueue[0].Hurt == 1);
        }

        [TestMethod]
        public void PlayerBoard_BurningDamage_1xAngry()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateOrc() });
            pb.Anger();
            pb.Damage(new Damage(1, false, true));
            Assert.IsTrue(pb.MonsterQueue[0].Hurt == 1);
        }

        [TestMethod]
        public void PlayerBoard_BurningDamage_2xNonAngry()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateOrc() });
            pb.Damage(new Damage(1, false, true));
            Assert.IsTrue(pb.MonsterQueue[0].Hurt == 2);
        }

        [TestMethod]
        public void PlayerBoard_ArrowDamage_SpillsToMonster()
        {
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateOrc(), MonsterFactory.CreateDemon() });
            pb.Damage(new Damage(5, true, false));
            Assert.IsTrue(pb.MonsterQueue[0].Hurt == 2);
        }

        [TestMethod]
        public void PlayerBoard_ArrowDamage_SpillsToPlayer()
        {
            //Board b = new Board(2);
            //PlayerBoard pb = b.Players[0];
            //PlayerBoard pb2 = b.Players[1];

            List<PlayerBoard> pbList = Factory_CreateLinkedPlayerBoards(2);
            PlayerBoard pb = pbList[0];
            PlayerBoard pb2 = pbList[1];

            pb.StartTurn(new List<MonsterCard>() { MonsterFactory.CreateOrc() });
            pb.Damage(new Damage(5, true, false));
            Assert.IsTrue(pb2.Life == 18);
        }

        private PlayerBoard Factory_CreateDefaultPlayerBoard()
        {
            return new PlayerBoard(Board.getBasicDraftDeck(), null, null);
        }

        private PlayerBoard Factory_CreateLinkedPlayerBoards(PlayerInteraction pi)
        {
            return new PlayerBoard(Board.getBasicDraftDeck(), null, pi);
        }

        private List<PlayerBoard> Factory_CreateLinkedPlayerBoards(int numBoards)
        {
            PlayerInteraction pi = new PlayerInteraction();
            List<PlayerBoard> pbList = new List<PlayerBoard>();
            for (int i = 0; i < numBoards; i++)
            {
                pbList.Add(Factory_CreateLinkedPlayerBoards(pi));
            }
            pi.AddPlayers(pbList);
            return pbList;
        }

        private void StartEmptyBoardTurn(PlayerBoard pb)
        {
            pb.StartTurn(new List<MonsterCard>());
        }

        private void StartBoardTurn(PlayerBoard pb)
        {
            pb.StartTurn(getFiveHellHounds());
        }

        private List<MonsterCard> getFiveHellHounds()
        {
            return Enumerable.Repeat(0, 5).Select(z => MonsterFactory.CreateHellhound()).ToList();
        }
    }
}
