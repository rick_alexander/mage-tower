﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tower;
using Tower.PlayerCards;
using Tower.PlayerCards.Cards;

namespace TowerTest
{
    [TestClass]
    public class GameStartTests
    {
        public List<PlayerCard> deck;
        [TestInitialize]
        public void Initialize()
        {
            this.deck = Board.getBasicDraftDeck();
        }

        [TestMethod]
        public void Motherlode_Gamestart()
        {
            deck.Add(new Motherlode());
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            Assert.IsTrue(pb.Gold == 3);
        }

        [TestMethod]
        public void Unicorn_Gamestart()
        {
            deck.Add(new Unicorn());
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            Assert.IsTrue(pb.Hand.Count == 3);
        }

        [TestMethod]
        public void MouthOfMadness_Gamestart()
        {
            deck.Add(new MouthOfMadness());
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            Assert.IsTrue(pb.Hand.Count == 4);
        }

        [TestMethod]
        public void UnholyChalice_Gamestart()
        {
            deck.Add(new UnholyChalice());
            PlayerBoard pb = Factory_CreateDefaultPlayerBoard();
            Assert.IsTrue(pb.Life == 23);
        }

        [TestMethod]
        public void SummoningRitual_Gamestart()
        {
            //I feel like I'm doing all sorts of wrong here
            Board b = new Board(0);
            deck.Add(new SummoningRitual());
            PlayerBoard pb = Factory_CreatePlayerBoardWithGameBoard(b);
            Assert.IsFalse(pb.Deck.Any(z => z.Type == CardType.Knight));
            Assert.IsTrue(pb.Deck.Any(z => z.Attributes.HasFlag(CardAttribute.Defender)) 
                       || pb.Hand.Any(z => z.Attributes.HasFlag(CardAttribute.Defender)));
        }

        private PlayerBoard Factory_CreateDefaultPlayerBoard()
        {
            return new PlayerBoard(deck, null, null);
        }

        private PlayerBoard Factory_CreatePlayerBoardWithGameBoard(Board b)
        {
            return new PlayerBoard(deck, b, null);
        }
    }
}
