﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower
{
    public abstract class Card
    {
        //public abstract string Name { get; }
        public readonly string Name;

        public Card(string name)
        {
            this.Name = name;
        }
    }
}
