﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class KingsGuard : Defender
    {
        public override CardAttribute Attributes { get { return CardAttribute.Defender; } }

        private static readonly int COST = 3;
        private static readonly int POWER = 3;
        private static readonly int TOUGHNESS = 4;
        
        public KingsGuard() : base(CardType.KingsGuard, COST, POWER, TOUGHNESS)
        {
        }

        public override void OnPlay()
        {
            owner.Defenders.Add(new KingsGuardToken());
        }
    }
}
