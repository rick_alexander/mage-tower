﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class Unicorn : Defender
    {
        private static readonly int COST = 1;
        private static readonly int POWER = 2;
        private static readonly int TOUGHNESS = 3;

        public Unicorn() : base(CardType.Unicorn, COST, POWER, TOUGHNESS)
        {
        }

        public override void GameStart()
        {
            owner.Hand.Add(this);
        }
    }
}
