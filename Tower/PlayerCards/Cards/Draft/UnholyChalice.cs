﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class UnholyChalice : PlayerCard
    {
        public UnholyChalice() : base(CardType.UnholyChalice, 1)
        {
        }

        public override void GameStart()
        {
            owner.Life += 3;
        }
    }
}
