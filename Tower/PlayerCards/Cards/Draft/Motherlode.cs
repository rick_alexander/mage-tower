﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class Motherlode : PlayerCard
    {
        public Motherlode()
            : base(CardType.Motherlode, 0)
        {
        }

        public override void GameStart(){
            owner.Gold += 3;
        }
    }
}
