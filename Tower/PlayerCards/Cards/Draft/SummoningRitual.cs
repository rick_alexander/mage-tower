﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class SummoningRitual : PlayerCard
    {
        public SummoningRitual() : base(CardType.SummoningRitual, 4)
        {
        }

        public override void GameStart()
        {
            int position = owner.Deck.FindIndex(z => z.Type == CardType.Knight);
            owner.Deck.RemoveAt(position);
            PlayerCard pc = owner.Shared.DraftDeck.First(z => z.Attributes.HasFlag(CardAttribute.Defender) && z.Cost >= 3 && z.Cost <= 6);
            owner.Deck.Insert(position, pc);
        }
    }
}
