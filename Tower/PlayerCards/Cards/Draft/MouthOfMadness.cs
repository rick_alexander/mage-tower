﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class MouthOfMadness : PlayerCard
    {
        public MouthOfMadness() : base(CardType.Motherlode, 1)
        {
        }

        public override void GameStart()
        {
            owner.Draw(2);
        }
    }
}
