﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class KingsGuardToken : KingsGuard
    {
        public override void OnPlay()
        {            
        }

        public override void OnDeath()
        {
            owner.Defenders.Remove(this);
        }
    }
}
