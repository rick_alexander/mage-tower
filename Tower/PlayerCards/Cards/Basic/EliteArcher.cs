﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    class EliteArcher : PlayerCard
    {
        public EliteArcher() : base(CardType.EliteArcher, 4)
        {
        }

        public override void OnPlay()
        {
            Damage d = Damage.ArrowDamage(3);
            owner.Damage(d);
        }
    }
}
