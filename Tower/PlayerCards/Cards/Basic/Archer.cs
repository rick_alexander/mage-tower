﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class Archer : PlayerCard
    {
        public Archer() : base(CardType.Archer, 3)
        {

        }

        public override void OnPlay(){
            Damage d = Damage.ArrowDamage(3);
            owner.Damage(d);
        }
    }
}
