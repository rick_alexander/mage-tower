﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.PlayerCards.Cards
{
    public class Knight : Defender
    {
        private static readonly int COST = 3;
        private static readonly int POWER = 3;
        private static readonly int TOUGHNESS = 4;

        public Knight() : base(CardType.Knight, COST, POWER, TOUGHNESS)
        {
        }
    }
}
