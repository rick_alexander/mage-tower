﻿using System;

namespace Tower
{
    [Flags]
    public enum CardAttribute
    {
        Unknown =   1 << 0,
        Ability =   1 << 1,
        Attack =    1 << 2,
        Boon =      1 << 3,
        Defender =  1 << 4,
        Equipment = 1 << 5,
        Permanant = 1 << 6,
        Basic =     1 << 7,
        Prize =     1 << 8
    }
}
