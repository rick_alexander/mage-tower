﻿using System.ComponentModel;

namespace Tower
{
    public enum CardType
    {
        Unknown,
        #region Main Cards
        //Basic
        Archer,
        [Description("Elite Archer")]
        EliteArcher,
        Knight,
        //Prize
        Summoning,
        Intellect,
        Gloryseeking,
        //Draft
        [Description("King's Guard")]
        KingsGuard,
        Motherlode,
        [Description("Mouth of Madness")]
        MouthOfMadness,
        [Description("Summoning Ritual")]
        SummoningRitual,
        [Description("Unholy Chalice")]
        UnholyChalice,
        Unicorn,
        //Token and Confusion
        Confusion,
        [Description("King's Guard Token")]
        KingsGuardToken,
        #endregion
        #region Co-op Invalids

        #endregion
    }
}
