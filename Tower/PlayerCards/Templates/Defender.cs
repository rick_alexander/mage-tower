﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tower.Monsters;

namespace Tower.PlayerCards
{
    public class Defender : PlayerCard
    {
        public int Power;
        public int Toughness;
        public int Hurt = 0;

        public Defender(CardType type, int Cost, int Power, int Toughness) : base(type, Cost)
        {
            this.Power = Power;
            this.Toughness = Toughness;
        }

        public virtual void OnBattle(){
            int retort;

            MonsterCard mc = owner.MonsterQueue.FirstOrDefault();
            if (mc != null)
            {
                retort = mc.Power;
            }
            else
            {
                retort = 2; //Hitting a player
            }

            owner.Damage(Damage.BasicDamage(Power));

            TakeDamage(retort);
        }

        public virtual void TakeDamage(int count)
        {
            Hurt += count;
            if (Hurt >= Toughness)
            {
                OnDeath();
            }
        }

        public virtual void OnDeath()
        {
            owner.Defenders.Remove(this);
            owner.Discard.Add(this);
        }
    }
}
