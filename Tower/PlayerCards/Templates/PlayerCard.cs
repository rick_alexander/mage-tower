﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tower.Exceptions;

namespace Tower.PlayerCards
{
    public abstract class PlayerCard : Card
    {
        public CardType Type;
        public virtual CardAttribute Attributes { get { return CardAttribute.Unknown; } }

        public PlayerBoard owner;
        public readonly int Cost;

        public PlayerCard(CardType type, int cost) : base(type.ToString())
        {
            this.Type = type;
            this.Cost = cost;
        }

        public virtual void GameStart()
        {
        }

        public virtual void Play()
        {
            CheckPlay();
            OnPlay();
        }

        public virtual void CheckPlay()
        {
        }

        public virtual void OnPlay()
        {
            throw new BadUsageException();
        }

        public virtual void OnDiscard()
        {
        }
    }
}
