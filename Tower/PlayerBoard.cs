﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tower.PlayerCards;
using Tower.Monsters;

namespace Tower
{
    public class PlayerBoard
    {
        public List<PlayerCard> Deck = new List<PlayerCard>();
        public List<PlayerCard> Discard = new List<PlayerCard>();
        public List<Permanant> Permanants = new List<Permanant>();
        public List<Defender> Defenders = new List<Defender>();
        public List<PlayerCard> Hand = new List<PlayerCard>();
        public List<MonsterCard> MonsterQueue = new List<MonsterCard>();

        private PlayerInteraction OtherPlayers;
        public Board Shared { get; set; }

        public int Life { get; set; }

        public int Gold { get; set; }

        public PlayerBoard(List<PlayerCard> draft, Board shared, PlayerInteraction pi)
        {
            Deck = draft;
            Shared = shared;
            OtherPlayers = pi;
            Life = 20;

            ResolveGameStarts();

            Shuffle();
            Draw(2);
        }

        private void ResolveGameStarts()
        {
            //Summoning Ritual and Gelatinous Cube modify the collection, which causes an error
            //foreach (PlayerCard c in Deck)
            for(int i = 0; i < Deck.Count; i++)
            {
                PlayerCard c = Deck[i];
                c.owner = this;
                c.GameStart();
            }
            foreach (PlayerCard c in Hand) //In case a Gamestart ability added to hand (And couldn't modify the Deck collection while it was iterating)
            {
                Deck.Remove(c);
            }
        }

        public void Draw(int x)
        {
            for (int i = 0; i < x; i++)
            {
                DrawOne();
            }
        }

        private void DrawOne()
        {
            if (Deck.Count == 0)
            {
                Shuffle();
                if(Deck.Count == 0){
                    return; //All cards are in hand/play. Cannot draw.
                }
            }
            PlayerCard pc = Deck[0];
            Hand.Add(pc);
            Deck.Remove(pc);
        }

        private void Shuffle()
        {
            Deck.Shuffle();
        }

        public void StartTurn(List<MonsterCard> monsters)
        {
            MonsterQueue.AddRange(monsters);
            int x = getDrawCount();
            Draw(x);
            Gold += 1;
        }

        private int getDrawCount()
        {
            int drawCount = 2;
            foreach(Permanant p in Permanants){
                drawCount += p.getDrawModifier();
            }
            foreach(MonsterCard c in MonsterQueue){

            }
            return drawCount;
        }

        public void Anger()
        {
            //Demonsspawn modify the collection, which causes an error
            //foreach (MonsterCard mc in Queue)
            //{
            //    mc.Anger(this);
            //}
            for (int i = 0; i < MonsterQueue.Count; i++)
            {
                MonsterQueue[i].Anger(this);
            }
            MonsterQueue = MonsterQueue.Where(z => z.AngerCount > 0).ToList();
        }

        public void Damage(Damage damage)
        {
            if (MonsterQueue.Count == 0)
            {
                OtherPlayers.Damage(damage.Count, this);
            }
            else
            {
                Damage(MonsterQueue[0], damage);
            }
        }

        public void Damage(MonsterCard mc, Damage damage)
        {
            mc.Damage(this, damage);
        }
    }
}
