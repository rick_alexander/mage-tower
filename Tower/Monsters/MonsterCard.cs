﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.Monsters
{
    public class MonsterCard : Card
    {
        public readonly int Value;
        public int Power;
        public int Toughness;
        public int Hurt;

        //private delegate void Anger(PlayerBoard pb);
        private Action<PlayerBoard> AngerAction = (x) => { }; //Do nothing
        public int AngerCount { get; private set; }

        public MonsterCard(string name, int Value, int Power, int Toughness, int AngerCount, Action<PlayerBoard> AngerAction)
            : this(name, Value, Power, Toughness, AngerCount)
        {
            this.AngerAction = AngerAction;
        }

        public MonsterCard(string name, int Value, int Power, int Toughness, int AngerCount)
            : base(name)
        {
            this.Value = Value;
            this.AngerCount = AngerCount;
            this.Power = Power;
            this.Toughness = Toughness;
        }

        public void Anger(PlayerBoard pb){
            AngerCount--;

            if (AngerCount == 0)
            {
                pb.Life -= this.Power;
            }
            else if (AngerCount == 1)
            {
                AngerAction(pb);
            }
        }

        public void Damage(PlayerBoard pb, Damage d)
        {
            if (d.isBurning && AngerCount > 1)
            {
                d.Count *= 2;
            }
            
            int lifeRemaining = (Toughness - Hurt);
            
            Hurt += d.Count;

            //if (Health >= Toughness)
            if(d.Count >= lifeRemaining)
            {
                pb.MonsterQueue.Remove(this); //Die
                if (d.isArrow && (d.Count > lifeRemaining)) //GT this time, not GTE
                {
                    d.Count -= lifeRemaining;
                    pb.Damage(d);
                }
            }
        }

        public void OnQueue() //I.e. Harpy
        {
        }

        public void OnDeath()
        {
        }
    }
}