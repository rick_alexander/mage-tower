﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower.Monsters
{
    public static class MonsterFactory
    {
        public static MonsterCard CreateDemonspawn()
        {
            return new MonsterCard("Demonspawn", 0, 1, 1, 2);
        }
        public static MonsterCard CreateHellhound()
        {
            return new MonsterCard("Hellhound", 2, 1, 2, 1);
        }
        public static MonsterCard CreateOrc()
        {
            return new MonsterCard("Orc", 2, 2, 3, 2);
        }
        public static MonsterCard CreateOgre()
        {
            return new MonsterCard("Ogre", 3, 3, 4, 2);
        }
        public static MonsterCard CreateWarlock()
        {
            return new MonsterCard("Warlock", 3, 2, 2, 2, (pb) => pb.Life--);
        }
        public static MonsterCard CreateDemon()
        {
            return new MonsterCard("Demon", 5, 4, 4, 2, (pb) => pb.MonsterQueue.Add(MonsterFactory.CreateDemonspawn()));
        }
        public static MonsterCard CreateGiant()
        {
            return new MonsterCard("Giant", 5, 5, 5, 2);
        }
    }
}
