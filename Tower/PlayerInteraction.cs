﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tower.PlayerCards;

namespace Tower
{
    public class PlayerInteraction
    {
        List<PlayerBoard> Players = new List<PlayerBoard>();

        public PlayerInteraction()
        {

        }

        public void AddPlayer(PlayerBoard pb)
        {
            this.Players.Add(pb);
        }

        public void AddPlayers(List<PlayerBoard> Players){
            this.Players.AddRange(Players);
        }

        internal void Damage(int damage, PlayerBoard effectOwner)
        {
            var Targets = removeOwner(effectOwner);
            foreach (PlayerBoard pb in Targets)
            {
                pb.Life -= damage;
            }
        }

        private List<PlayerBoard> removeOwner(PlayerBoard effectOwner)
        {
            return Players.Except(new List<PlayerBoard>() { effectOwner }).ToList();
        }
    }
}
