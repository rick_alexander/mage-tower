﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tower.Monsters;
using Tower.PlayerCards;
using Tower.PlayerCards.Cards;

namespace Tower
{
    public class Board
    {
        private int StartingPlayers;
        private int ActivePlayers;

        public Stack<MonsterCard> MonsterDeck;// = new Stack<MonsterCard>();
        public List<MonsterCard> MonsterDiscard = new List<MonsterCard>();
        public Stack<PlayerCard> DraftDeck;
        public List<PlayerCard> DraftDiscard = new List<PlayerCard>();
        public List<Summoning> SummoningPrizePile;

        public List<PlayerBoard> Players = new List<PlayerBoard>();
        public int PlayerTurn = 0;

        public Board(int players)
        {
            InitializeDraftDeck();
            InitializeMonsterDeck();

            this.StartingPlayers = players;
            this.ActivePlayers = players;

            PlayerInteraction pi = new PlayerInteraction();

            for (int i = 1; i <= players; i++)
            {
                Players.Add(new PlayerBoard(getBasicDraftDeck(), this, pi));
            }

            pi.AddPlayers(Players);

            //Players.AddRange(Enumerable.Repeat(0, players).Select(z => new PlayerBoard(getBasicDraftDeck())));
        }

        public static List<PlayerCard> getBasicDraftDeck()
        {
            List<PlayerCard> deck = new List<PlayerCard>();
            deck.Add(new Archer());
            deck.Add(new Archer());
            deck.Add(new EliteArcher());
            deck.Add(new EliteArcher());
            deck.Add(new Knight());
            return deck;
        }

        public void startPlayerTurn()
        {
            PlayerBoard pb = getNextPlayer();
            pb.Anger();
            if (pb.Life > 0)
            {
                List<MonsterCard> monsters = get10PlusPoints();
                pb.StartTurn(monsters);
            }
            else
            {
                DeclareDead(pb);
            }
        }

        private PlayerBoard getNextPlayer()
        {
            PlayerTurn = PlayerTurn % ActivePlayers;
            PlayerTurn++;
            return Players[PlayerTurn];
        }

        //public Player NextPlayer(Player player)
        //{
        //    int i = playerList.IndexOf(player);
        //    do
        //    {
        //        if (++i == playerList.Count)
        //            i = 0;
        //    }
        //    while (!playerList[i].IsAlive);
        //    return playerList[i];
        //}

        private void DeclareDead(PlayerBoard pb)
        {
            throw new NotImplementedException();
        }

        private List<PlayerBoard> OtherPlayers(PlayerBoard pb)
        {
            return Players.Except( new List<PlayerBoard>(){ pb }).ToList();
        }

        private List<MonsterCard> get10PlusPoints()
        {
            MonsterCard mc;
            List<MonsterCard> monsters = new List<MonsterCard>();
            int value = 0;

            while (value < 10)
            {
                mc = DrawMonster();
                if (value + mc.Value < 13)
                {
                    monsters.Add(mc);
                    value += mc.Value;
                }
                else
                {
                    MonsterDiscard.Add(mc);
                }
            }
            return monsters;
        }

        private MonsterCard DrawMonster()
        {
            MonsterCard mc = MonsterDeck.Pop();
            if (MonsterDeck.Count == 0)
            {
                MonsterDeck = ShuffleAny(MonsterDeck, ref MonsterDiscard);
            }
            return mc;
        }

        private Stack<T> ShuffleAny<T>(Stack<T> Deck, ref List<T> Discard)
        {
            Discard.Shuffle();
            Deck = new Stack<T>(Discard);
            Discard.Clear();
            return Deck;
        }

        private void InitializeMonsterDeck()
        {
            MonsterDiscard.AddRange(Enumerable.Repeat(0, 8).Select(x => MonsterFactory.CreateHellhound()));
            MonsterDiscard.AddRange(Enumerable.Repeat(0, 8).Select(x => MonsterFactory.CreateOrc()));
            MonsterDiscard.AddRange(Enumerable.Repeat(0, 8).Select(x => MonsterFactory.CreateOgre()));
            MonsterDiscard.AddRange(Enumerable.Repeat(0, 8).Select(x => MonsterFactory.CreateWarlock()));
            MonsterDiscard.AddRange(Enumerable.Repeat(0, 8).Select(x => MonsterFactory.CreateGiant()));
            MonsterDiscard.AddRange(Enumerable.Repeat(0, 8).Select(x => MonsterFactory.CreateDemon()));
        }

        private void InitializeDraftDeck()
        {
            DraftDiscard.Add(new KingsGuard());
            DraftDiscard.Add(new Motherlode());
            DraftDiscard.Add(new MouthOfMadness());
            DraftDiscard.Add(new SummoningRitual());
            DraftDiscard.Add(new UnholyChalice());
            DraftDiscard.Add(new Unicorn());

            DraftDeck = ShuffleAny(DraftDeck, ref DraftDiscard);
        }
    }
}