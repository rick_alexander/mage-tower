﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tower
{
    public class Damage
    {
        public readonly bool isBurning;
        public readonly bool isArrow;
        public int Count;

        public static Damage BasicDamage(int Count)
        {
            return new Damage(Count);
        }

        public static Damage ArrowDamage(int Count)
        {
            return new Damage(Count, true);
        }

        public static Damage BurningDamage(int Count)
        {
            return new Damage(Count, false, true);
        }

        public Damage(int Count, bool Arrow, bool Burning)
        {
            this.Count = Count;
            this.isArrow = Arrow;
            this.isBurning = Burning;

            if (Arrow && Burning)
            {
                throw new Exception("Damage cannot be of both Burning and Arrow types");
            }
        }

        public Damage(int Count, bool Arrow) : this(Count, Arrow, false)
        {
        }

        public Damage(int Count) : this(Count, false, false)
        {
        }
    }
}
